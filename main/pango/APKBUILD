# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=pango
pkgver=1.45.4
pkgrel=0
pkgdesc="A library for layout and rendering of text"
url="https://www.pango.org/"
arch="all"
license="LGPL-2.1-or-later"
depends_dev="pango-tools=$pkgver-r$pkgrel"
makedepends="meson cairo-dev expat-dev gobject-introspection-dev help2man
	fontconfig-dev glib-dev harfbuzz-dev libxft-dev fribidi-dev
	gtk-doc"
checkdepends="ttf-dejavu ttf-cantarell ttf-droid ttf-tlwg"
install="$pkgname.pre-deinstall"
triggers="$pkgname.trigger=/usr/lib/pango/*/modules"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-doc $pkgname-tools"
source="https://download.gnome.org/sources/pango/${pkgver%.*}/pango-$pkgver.tar.xz
	"

# secfixes:
#   1.44.1-r0:
#     - CVE-2019-1010238

build() {
	meson  \
		--prefix=/usr \
		--buildtype=plain \
		-Dintrospection=true \
		-Dgtk_doc=true \
		build
	ninja -C build
}

check() {
	ninja -C build test
}

package() {
	DESTDIR="$pkgdir" ninja -C build install
}

tools() {
	pkgdesc="$pkgdesc (tools)"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/* "$subpkgdir"/usr/bin
}

sha512sums="3a3540c558d727e3f4e2836dd0fcbdc078e8d268941014db8eddd7b720ff64ad56a74328771888fce911f3c8b015df34a293e6b068cb43f974086b778a3b87f3  pango-1.45.4.tar.xz"
